import { CommonRoutesConfig } from '../common/common.routes.config';
import UsersController from './controllers/users.controller';
import UsersMiddleware from './middleware/users.middleware';
import express from 'express';



// The body() method will validate fields and generate an errors list�stored in the express.
import { body } from 'express-validator';
// We then need our own middleware to check and make use of the errors list.
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';

export class UsersRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'UsersRoutes');
    }

    configureRoutes(): express.Application {

        this.app
            .route(`/users`)
            .get(UsersController.listUsers)
            // CREATE NEW USER
            .post(
                body('email').isEmail()
                    .withMessage('Must include password (5+ characters)'),// Use body() validator to check email
                body('password')                // Use body() validator to check password.
                    .isLength({ min: 5 })
                    .withMessage('Must include password (5+ characters)'),
                BodyValidationMiddleware.verifyBodyFieldsErrors,    // Use BodyValidationMiddleware to check errors in fields.
                UsersMiddleware.validateSameEmailDoesntExist,
                UsersController.createUser
            );

        // Extract user id and add to the request body.
        this.app.param(`userId`, UsersMiddleware.extractUserId);



        this.app
            .route(`/users/:userId`)
            .all(UsersMiddleware.validateUserExists) // this middleware function runs before any request to /users/:userId (GET, PUT, PATCH, or DELETE)
            .get(UsersController.getUserById)
            .delete(UsersController.removeUser);


        this.app.put(`/users/:userId`, [
            body('email').isEmail(),
            body('password')
                .isLength({ min: 5 })
                .withMessage('Must include password (5+ characters)'),
            body('firstName').isString(),
            body('lastName').isString(),
            body('permissionFlags').isInt(),
            BodyValidationMiddleware.verifyBodyFieldsErrors,
            UsersMiddleware.validateSameEmailBelongToSameUser,
            UsersController.put,
        ]);


        this.app.patch(`/users/:userId`, [
            body('email').isEmail().optional(),
            body('password')
                .isLength({ min: 5 })
                .withMessage('Password must be 5+ characters')
                .optional(),
            body('firstName').isString().optional(),
            body('lastName').isString().optional(),
            body('permissionFlags').isInt().optional(),
            BodyValidationMiddleware.verifyBodyFieldsErrors,
            UsersMiddleware.validatePatchEmail,
            UsersController.patch,
        ]);

        return this.app;
    }
}


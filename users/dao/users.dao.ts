// Import connection to MongoDB.
import mongooseService from '../../common/services/mongoose.service';
import debug from 'debug';
import shortid from "shortid";
// Import DTOs.
import { CreateUserDto }    from '../dto/create.user.dto';
import { PatchUserDto }     from '../dto/patch.user.dto';
import { PutUserDto }       from '../dto/put.user.dto';


const log: debug.IDebugger = debug('app:in-memory-dao');

class UsersDao {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // USERS SCHEMA FOR MONGOOSE ///////////////////////////////////////////////////////////////////////////////////////
    // User Schema for Mongoose: The user schema defines which fields should exist in our MongoDB collection called Users,
    // while the DTO entities defines which fields to accept in an HTTP request.
    Schema = mongooseService.getMongoose().Schema;
    userSchema = new this.Schema({
        _id: String,
        email: String,
        password: { type: String, select: false }, // The select: false in the password will hide this field whenever we get a user or list all users.
        firstName: String,
        lastName: String,
        permissionFlags: Number,
    }, { id: false }); // Mongoose models provide a virtual id getter by default, so we�ve disabled that option above with { id: false }

    User = mongooseService.getMongoose().model('Users', this.userSchema);
    // END OF USERS SCHEMA FOR MONGOOSE ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Class constructor
     */
    constructor() {
        log('Created new instance of UsersDao');
    }

    // CREATE USER
    async addUser(userFields: CreateUserDto) {
        const userId = shortid.generate();
        // Instantiate schema. Whatever the API consumer sends in for permissionFlags via userFields,
        // we then override it with the value 1.
        const user = new this.User({_id: userId, ...userFields, permissionFlags: 1});
        await user.save();
        return userId;
    }

    // READ USER
    // Each of the calls to User member functions is chained to an exec() call. This is optional,
    // but the Mongoose developers recommend it because it provides better stack traces when debugging.
    async getUserByEmail(email: string) {return this.User.findOne({ email: email }).exec()}

    async getUserById(userId: string) {
        return this.User.findOne({ _id: userId }).populate('User').exec();
    }

    async getUsers(limit = 25, page = 0) {
        return this.User.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }


    // UPDATE USER
    /**
     * A single DAO function will suffice because the underlying Mongoose findOneAndUpdate() function can update the
     * entire document or just part of it. Note that our own function will take userFields as either a PatchUserDto or a
     * PutUserDto, using a TypeScript union type (signified by |)
     *
     * @param userId
     * @param userFields
     */
    async updateUserById(userId: string, userFields: PatchUserDto | PutUserDto) {
        const existingUser = await this.User.findOneAndUpdate(
            { _id: userId },
            { $set: userFields },
            { new: true } // The new: true option tells Mongoose to return the object as it is after the update, rather than how it originally had been.
        ).exec();

        return existingUser;
    }


    // DELETE USER
    async removeUserById(userId: string) {return this.User.deleteOne({ _id: userId }).exec()}


}


// Using the singleton pattern, this class will always provide the same instance
export default new UsersDao();
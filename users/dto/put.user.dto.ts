export interface PutUserDto {
    // id: string; // Mongoose automatically makes an _id field available
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    permissionFlags: number;
}
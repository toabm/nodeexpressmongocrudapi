/**
 * DTO contains just the fields that we want to pass between the API client and our database.
 */
export interface CreateUserDto {
    // id: string; // Mongoose automatically makes an _id field available
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
    permissionFlags?: number;
}
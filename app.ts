// https://www.toptal.com/express-js/nodejs-typescript-rest-api-pt-1
// https://www.toptal.com/express-js/nodejs-typescript-rest-api-pt-2
// https://www.toptal.com/express-js/nodejs-typescript-rest-api-pt-3

import express from 'express';
import * as http from 'http';
import * as winston from 'winston';
import * as util from 'util';      // To print objects on console.
import * as expressWinston from 'express-winston';
import cors from 'cors';
import {CommonRoutesConfig} from './common/common.routes.config';
import {UsersRoutes} from './users/users.routes.config';
import debug from 'debug';



const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = 3000;
const routes: Array<CommonRoutesConfig> = []; // Array of routes
const debugLog: debug.IDebugger = debug('app');


// Use cors: middleware to allow cross-origin requests
app.use(cors());

// Middleware to parse all incoming requests as JSON objects
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(express.json({limit: '50mb'}));

// MIDDLEWARE TO PRINT PARAMS FOR EVERY REQUEST.
app.use(function (req, res, next) {
    // Print request path on cyan color.
    console.log('\x1b[36m%s\x1b[0m', "-----------------------------------------------------------------------------------------------");
    console.log('\x1b[36m%s\x1b[0m', req.method + ' REQUEST --> ' + req.originalUrl);
    console.log('\x1b[36m%s\x1b[0m', "-----------------------------------------------------------------------------------------------");

    // Print path params on console.
    console.log("\x1b[0m", "-- Path Params ------------------------------------------------");
    Object.entries(req.params).length === 0 ? console.log("\x1b[35m", "\tNO HAY DATOS") : console.log(`\t${util.inspect(req.params, false, null, true)}`);
    // Print URL Query params.
    console.log("\x1b[0m", "-- Query Params -----------------------------------------------");
    Object.entries(req.query).length === 0 ? console.log("\x1b[35m", "\tNO HAY DATOS") : console.log(`\t${util.inspect(req.query, false, null, true )}`);
    // Print POST params, request body object.
    console.log("\x1b[0m", "-- Post Params ------------------------------------------------");
    Object.entries(req.body).length === 0 ? console.log("\x1b[35m", "\tNO HAY DATOS") : console.log(`\t${util.inspect(req.body, false, null, true )}`);

    next();
});


// here we are preparing the expressWinston logging middleware configuration,
// which will automatically log all HTTP requests handled by Express.js
const loggerOptions: expressWinston.LoggerOptions = {
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.colorize({ all: true })
    ),
};
if (!process.env.DEBUG) {
    loggerOptions.meta = false; // when not debugging, log requests as one-liners
}
// initialize the logger with the above configuration
app.use(expressWinston.logger(loggerOptions));


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SERVER ROUTES ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Here we are adding the UserRoutes to our array,
// after sending the Express.js application object to have the routes added to our app!
routes.push(new UsersRoutes(app));

// this is a simple route to make sure everything is working properly
const runningMessage = `Server running at http://localhost:${port}`;
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(runningMessage)
});
// END OF SERVER ROUTES ////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// START SERVER
server.listen(port, () => {
    routes.forEach((route: CommonRoutesConfig) => {debugLog(`Routes configured for ${route.getName()}`)});
    // Our exception to avoiding console.log(), because we always want to know when the server is done starting up
    console.log(runningMessage);
});